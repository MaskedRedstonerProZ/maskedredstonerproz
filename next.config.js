/** @type {import('next').NextConfig} */
const nextConfig = {
    async rewrites() {
        return [
            {
                source: "/MaskedRedstonerProZ/app-data/-/raw/main/app-data.json",
                destination: "https://gitlab.com/MaskedRedstonerProZ/app-data/-/raw/main/app-data.json",
            },
        ]
    },
    images: {
        remotePatterns: [
            {
                protocol: "https",
                hostname: "gitlab.com"
            }
        ]
    }
};

module.exports = nextConfig
