import type { Config } from "tailwindcss";

const config: Config = {
    content: [
        "./src/pages/**/*.{js,ts,jsx,tsx,mdx}",
        "./src/components/**/*.{js,ts,jsx,tsx,mdx}",
        "./src/app/**/*.{js,ts,jsx,tsx,mdx}",
    ],
    theme: {
        extend: {
            backgroundImage: {
                "gradient-radial": "radial-gradient(var(--tw-gradient-stops))",
                "gradient-conic":
                    "conic-gradient(from 180deg at 50% 50%, var(--tw-gradient-stops))",
            },
            colors: {
                "standard-grey": "#30343F",
                "standard-white": "#F7F0F5 !important",
                "standard-red": "#C80000",
                "standard-red-important": "#C80000 !important",
                "standard-blue": "#6184D8 !important"
            },
            keyframes: {
                spinMulti: {
                    '0%': { transform: 'rotate(0deg)' },
                    '100%': { transform: 'rotate(360deg)' },
                },
            },
            animation: {
                spinMulti: 'spinMulti 0.7s linear', // 1.5s for a reasonably fast triple spin
            },
            screens: {
                xs: "450px"
            }
        },
        data: {
            "active": "active~=true"
        }
    },
    plugins: [],
    corePlugins: {
        preflight: false
    }
};
export default config;
