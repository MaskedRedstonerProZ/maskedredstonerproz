import prisma from "@/lib/prisma";
import { ProjectDataPage, projectDataInclude } from "@/lib/types";
import apiCatchBlock from "@/util/apiCatchBlock";
import { ProjectType } from "@prisma/client";
import { NextRequest } from "next/server";

interface ProjectDataRequest {
    id?: string
    name: string,
    description: string,
    logoUrl: string,
    type?: ProjectType,
    gitlabUrl: string,
    accessUrl?: string
}

export async function POST(
    req: Request
) {
    try {

        const request: ProjectDataRequest = await req.json();

        const projectDataResponse = await prisma.projectData.upsert({
            where: {
                id: request.id || ""
            },
            create: request,
            update: request,
            select: {
                id: true,
                type: true
            }
        });

        return Response.json({ id: projectDataResponse.id, type: projectDataResponse.type });
    } catch (error) {
        return apiCatchBlock(error as Error);
    }
}

export async function GET(req: NextRequest) {
    try {
        const cursor = req.nextUrl.searchParams.get("cursor") || undefined;

        const pageSize = 10;

        const projectDataList = await prisma.projectData.findMany({
            include: projectDataInclude,
            take: pageSize + 1,
            cursor: cursor ? { id: cursor } : undefined,
        });

        const nextCursor = projectDataList.length > pageSize ? projectDataList[pageSize].id : null;

        const data: ProjectDataPage = {
            projectData: projectDataList,
            nextCursor
        }

        return Response.json(data);
    } catch (error) {
        return apiCatchBlock(error as Error);
    }
}