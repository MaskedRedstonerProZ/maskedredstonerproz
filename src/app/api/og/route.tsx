import { ImageResponse } from "next/og";
import { NextRequest } from "next/server";

export const runtime = "edge"

const size = {
    width: 1200,
    height: 630,
}

async function loadOrbitronFont() {
    const url = `https://fonts.googleapis.com/css2?family=Orbitron:wght@600&display=swap`
    const css = await (await fetch(url)).text()
    const resource = css.match(/src: url\((.+)\) format\('(opentype|truetype)'\)/)
   
    if (resource) {
      const response = await fetch(resource[1])
      if (response.status == 200) {
        return await response.arrayBuffer()
      }
    }
   
    throw new Error('failed to load font data')
  }

export async function GET(req: NextRequest) {

    return new ImageResponse(
        (
            <div
                tw="flex flex-row w-full h-full bg-[#30343F] text-white p-20"
            >
                    <div
                        tw="flex flex-row w-full h-full justify-center items-center bg-black rounded-xl p-5"
                    >
                        <img
                            src={`${req.url.split("/").slice(0, -2).join("/")}/logo.svg`}
                            alt="logo"
                            width={300}
                            height={300}
                        />
                        <div
                            tw="flex flex-col h-full justify-center p-10"
                        >
                            <h1 style={{ fontSize: "45px" }}>MaskedRedstonerProZ</h1>
                            <p style={{ fontSize: "30px" }}>An Android + web developer with a burning passion.</p>
                        </div>
                    </div>
            </div>
        ),
        {
            ...size,
            fonts: [
                {
                    name: "Orbitron",
                    data: await loadOrbitronFont(),
                    style: "normal"
                }
            ]
        }
    );
}