import prisma from "@/lib/prisma";
import apiCatchBlock from "@/util/apiCatchBlock";

interface AndroidScreenDescriptionRequest {
    id?: string,
    screenName: string,
    screenDescription: string,
    androidAppDataId?: string
}

export async function POST(req: Request) {
    try {
        const request: AndroidScreenDescriptionRequest = await req.json();

        const androidScreenDescriptionResponse = await prisma.androidScreenDescription.upsert({
            where: {
                id: request.id || ""
            },
            create: request,
            update: request,
            select: {
                id: true,
            }
        });

        return Response.json({ id: androidScreenDescriptionResponse.id });
    } catch(error) {
        return apiCatchBlock(error as Error);
    }
}