import prisma from "@/lib/prisma";
import apiCatchBlock from "@/util/apiCatchBlock";

interface AndroidAppDataRequest {
    id?: string,
    screenshotUrls?: string[],
    projectDataId: string
}

export async function POST(req: Request) {
    try {
        const request: AndroidAppDataRequest = await req.json();

        const androidAppDataResponse = await prisma.androidAppData.upsert({
            where: {
                id: request.id || ""
            },
            create: request,
            update: request,
            select: {
                id: true
            }
        });

        return Response.json({ id: androidAppDataResponse.id });
    } catch(error) {
        return apiCatchBlock(error as Error);
    }
}