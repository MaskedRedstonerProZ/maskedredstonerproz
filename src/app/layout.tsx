import type { Metadata, Viewport } from "next";
import { Orbitron } from "next/font/google";
import "./globals.css";
import { MantineProvider, useMantineColorScheme } from "@mantine/core";
import "@mantine/core/styles.css";
import clsx from "clsx";
import { Analytics } from "@vercel/analytics/next";
import ScrollToTopButton from "@/components/ScrollToTopButton";
import ReactQueryProvider from "./ReactQueryProvider";

const font = Orbitron({ weight: "400", subsets: ["latin"] });

export const viewport: Viewport = {
    width: "device-width",
    initialScale: 1
}

export const metadata: Metadata = {
    title: "MaskedRedstonerProZ",
    description: "An Android + web developer with a burning passion.",
    openGraph: {
        images: [
            "./api/og"
        ],
    },
    metadataBase: new URL("https://maskedredstonerproz.vercel.app")
};

export default function RootLayout({
    children,
}: Readonly<{
    children: React.ReactNode;
}>) {
    return (
        <html lang="en">
            <head>
                <meta
                    property="og:image:width"
                    content="1200px"
                />
                <meta
                    property="og:image:height"
                    content="630px"
                />
            </head>
            <body
                className={
                    clsx(
                        "bg-standard-grey text-standard-white touch-pan-y w-screen",
                        font.className
                    )
                }
            >
                <MantineProvider>
                    <ReactQueryProvider>
                        <Analytics />
                        <main>
                            {children}
                        </main>
                        <ScrollToTopButton />
                    </ReactQueryProvider>
                </MantineProvider>
            </body>
        </html>
    );
}
