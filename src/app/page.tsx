import ContactFooter from "@/components/root/ContactFooter";
import Header from "@/components/root/Header";
import ProjectDataList from "@/components/root/project_data/ProjectDataList";
import StandardPageRoot from "@/components/StandardPageRoot";
import { Flex } from "@mantine/core";

export default function Home() {
    return (
        <StandardPageRoot>
            <Flex
                className="max-w-full xl:max-w-[75%] 2xl:max-w-[50%]"
                direction="column"
                gap={30}
            >
                <Header />
                <ProjectDataList />
                <ContactFooter />
            </Flex>
        </StandardPageRoot>
    );
}
