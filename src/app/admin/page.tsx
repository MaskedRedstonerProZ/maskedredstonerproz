import StandardPageRoot from "@/components/StandardPageRoot";
import AdminFormList from "@/components/admin/AdminFormList";
import { Center, Flex, Space } from "@mantine/core";
import { cookies } from "next/headers";
import { redirect } from "next/navigation";

export default async function Admin() {

    if((await cookies()).get("admin_access_secret")?.value !== process.env.ADMIN_ACCESS_SECRET) {
        console.log(process.env.ADMIN_ACCESS_SECRET);
        redirect("/");
    }
    
    return (
        <StandardPageRoot>
            <Flex
                direction="column"
            >
                <Center>
                    <h1 className="bg-black py-5 px-10 rounded-xl shadow-xl shadow-gray-950 select-none max-w-fit" >ADMIN</h1>
                </Center>
                <Space h={20} />
                <AdminFormList />
            </Flex>
        </StandardPageRoot>
    );
}