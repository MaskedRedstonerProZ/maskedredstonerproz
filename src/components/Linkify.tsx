import Link from "next/link";
import { PropsWithChildren } from "react";
import { LinkIt } from "react-linkify-it";

interface LinkifyProps extends PropsWithChildren {}

export default function Linkify({ children }: LinkifyProps) {
	return (
		<LinkIt
			component={(match, key) => (
				<Link
					key={key}
                    className="text-standard-red no-underline hover:underline"
					href={`https://instagram.com/${match.slice(1)}`}
                    target="_blank"
                    rel="noopener noreferer"
				>
					{match}
				</Link>
			)}
			regex={/(@[a-zA-Z0-9_-]+)/}
		>
			{children}
		</LinkIt>
	);
}