import clsx from "clsx";
import Image from "next/image";

interface LogoProps {
    className?: string
}

export default function Logo({ className }: LogoProps) {
    return (
        <Image
            className={clsx(className)}
            src="logo.svg"
            alt="logo"
            width={250}
            height={250}
        />
    );
}