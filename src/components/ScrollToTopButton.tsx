"use client";

import { rem } from "@mantine/core";
import { IconArrowUp } from "@tabler/icons-react";
import React, { useEffect, useState } from "react";
import StandardTooltip from "./StandardTooltip";

export default function ScrollToTopButton() {
    const [isVisible, setIsVisible] = useState(false);

    function handleScrollToTop() {
        window.scrollTo({
            top: 0,
            behavior: "smooth",
        });
    }

    useEffect(() => {
        function handleScroll() {
            const scrollTop = window.scrollY || document.documentElement.scrollTop;
            setIsVisible(scrollTop > 300);
        }

        window.addEventListener("scroll", handleScroll);
        return () => {
            window.removeEventListener("scroll", handleScroll);
        };
    }, []);

    return (
        <>
            {isVisible && (
                <div
                    className="fixed bottom-20 right-8 z-50"
                >
                    <StandardTooltip
                        label="Scroll to top"
                        altColour
                    >
                        <div
                            onClick={handleScrollToTop}
                            className="flex flex-row items-center justify-center w-12 h-12 bg-standard-grey group rounded-full shadow-lg shadow-gray-950 cursor-pointer hover:bg-black transition-colors xl:border-5 xl:border-black xl:border-solid"
                        >
                            <IconArrowUp className="text-black group-hover:text-white" style={{ height: rem(28), width: rem(28) }} stroke={3.5} />
                        </div>
                    </StandardTooltip>
                </div>
            )}
        </>
    );
}
