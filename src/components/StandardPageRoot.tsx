import { Center } from "@mantine/core";

interface StandardPageRootProps extends React.PropsWithChildren {}

export default function StandardPageRoot({ children }: StandardPageRootProps) {
    return (
        <Center
            className="px-5 xs:px-10 py-5"
        >
            {children}
        </Center>
    );
}