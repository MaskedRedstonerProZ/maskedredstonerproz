import { Text, Textarea } from "@mantine/core";
import { Control, Controller, FieldError } from "react-hook-form";

interface StandardAdminTextareaProps {
    name: string,
    label?: string,
    placeholder?: string,
    required?: boolean,
    control: Control<any, any>,
    error?: FieldError
}

export default function StandardAdminTextarea({ name, label, placeholder, required = false, control, error  }: StandardAdminTextareaProps) {
    return (
        <Controller
            name={name}
            control={control}
            render={({ field }) => (
                <Textarea
                    classNames={
                        {
                            input: "bg-black text-standard-white border-standard-red focus:border-standard-red",
                            section: "bg-black text-standard-white"
                        }
                    }
                    label={label}
                    placeholder={placeholder}
                    {...field}
                    resize="vertical"
                    required={required}
                    error={error?.message}
                />
            )}
        />
    );
}