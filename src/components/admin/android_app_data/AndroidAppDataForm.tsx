"use client";

import { Center, Flex, Space, } from "@mantine/core";
import ResponsiveTitle from "../../root/ResponsiveTitle";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { AndroidAppDataValues, androidAppDataSchema } from "@/lib/validation";
import { useCreateUpdateAndroidAppData } from "@/hooks/data/creation+updating/useCreateUpdateAndroidAppData";
import StandardAdminFieldset from "../StandardAdminFieldset";
import AndroidAppDataScreenshotUrlInput from "./AndroidAppDataScreenshotUrlInput";

interface AndroidAppDataFormProps {
    androidAppDataId?: string,
    projectDataId?: string,
    onAndroidAppDataIdChange: (newId: string) => void
}

export default function AndroidAppDataForm({ androidAppDataId, projectDataId, onAndroidAppDataIdChange }: AndroidAppDataFormProps) {

    const { control, handleSubmit, reset, formState: { errors } } = useForm<AndroidAppDataValues>({
        resolver: zodResolver(androidAppDataSchema),
        defaultValues: {
            screenshotUrls: [],
            projectDataId: "i"
        },
    });

    const androidAppDataMutation = useCreateUpdateAndroidAppData();

    function onSubmit(input: AndroidAppDataValues) {

        androidAppDataMutation.mutate(
            androidAppDataId ? { id: androidAppDataId, ...input, projectDataId: projectDataId!! } : { ...input, projectDataId: projectDataId!! },
            {
                onSuccess: androidAppDataId ? undefined : ({ id }) => onAndroidAppDataIdChange(id)
            }
        );
        reset();
    }

    return (
        <Flex
            className="bg-black p-5 rounded-xl shadow-xl shadow-gray-950"
            direction="column"
        >
            <Center>
                <ResponsiveTitle
                    title="ANDROID APP DATA"
                />
            </Center>
            <Space h={20} />
            <form
                onSubmit={handleSubmit(onSubmit)}
                onKeyDown={(e) => {
                    if(e.key === "Enter") {
                        e.preventDefault();
                    }
                }}
            >
                <StandardAdminFieldset
                    isPending={androidAppDataMutation.isPending}
                    submittingDisabled={!projectDataId}
                >
                    <AndroidAppDataScreenshotUrlInput
                        required
                        name="screenshotUrls"
                        label="Screenshot urls"
                        placeholder="Add your Screenshot urls here"
                        control={control}
                        error={errors.screenshotUrls}
                    />
                </StandardAdminFieldset>
            </form>
        </Flex>
    );
}