"use client";

import { ActionIcon, Center, Flex, Menu, Table, Text, TextInput, rem } from "@mantine/core";
import { IconPlus } from "@tabler/icons-react";
import { useState } from "react";
import { Control, Controller, FieldError, Merge } from "react-hook-form";

interface AndroidAppDataScreenshotUrlInputProps {
    name: string,
    label?: string,
    placeholder?: string,
    required?: boolean,
    control: Control<any, any>,
    error?: Merge<FieldError, [(FieldError | undefined)?, ...(FieldError | undefined)[]]>
}

export default function AndroidAppDataScreenshotUrlInput({ name, label, placeholder, control, error }: AndroidAppDataScreenshotUrlInputProps) {

    const [currentValue, setCurrentValue] = useState("");

    return (
        <Controller
            name={name}
            control={control}
            render={({ field }) => (
                <Menu
                    classNames={
                        {
                            dropdown: "bg-black p-0 border-standard-red",
                        }
                    }
                    width="target"
                    trigger="hover"
                >
                    <Menu.Target>
                        <TextInput
                            classNames={
                                {
                                    input: "bg-black text-standard-white border-standard-red focus:border-standard-red",
                                    section: "bg-black text-standard-white"
                                }
                            }
                            label={label}
                            placeholder={placeholder}
                            value={currentValue}
                            error={error?.message}
                            onChange={(e) => setCurrentValue(e.currentTarget.value)}
                            rightSection={
                                <ActionIcon
                                    className="flex flex-row items-center justify-center pl-1 h-full border-l border-l-standard-red border-r-transparent border-y-transparent border-solid bg-transparent hover:bg-transparent"
                                    onClick={() => {
                                        field.onChange([...(field.value as string[]), currentValue]);
                                        setCurrentValue("");
                                    }}
                                >
                                    <IconPlus style={{ height: rem(25), width: rem(25) }} stroke={1.5} />
                                </ActionIcon>
                            }
                            onKeyDown={(e) => {
                                if (e.key === "Enter") {
                                    field.onChange([...(field.value as string[]), currentValue]);
                                    setCurrentValue("");
                                }
                            }}
                        />
                    </Menu.Target>
                    {(field.value as string[]).length !== 0 && (
                        <Menu.Dropdown>
                            <Table
                                className="w-full"
                                classNames={
                                    {
                                        tr: "border-b border-solid border-b-transparent border-x-transparent border-t-standard-red"
                                    }
                                }
                            >
                                <Table.Thead>
                                    <Table.Tr className="border-t-transparent">
                                        <Table.Th>
                                            <Center>Id</Center>
                                        </Table.Th>
                                        <Table.Th>
                                            <Center>Url</Center>
                                        </Table.Th>
                                        <Table.Th></Table.Th>
                                    </Table.Tr>
                                </Table.Thead>
                                <Table.Tbody>
                                    {(field.value as string[]).map((screenshotUrl, i) => (
                                        <Table.Tr
                                            key={i}
                                            className="bg-black text-standard-white cursor-default"
                                        >
                                            <Table.Td className="tabular-nums"><Center>{i + 1}.</Center></Table.Td>
                                            <Table.Td><Center>{screenshotUrl}</Center></Table.Td>
                                        </Table.Tr>
                                    ))}
                                </Table.Tbody>
                            </Table>
                        </Menu.Dropdown>
                    )}
                </Menu>
            )}
        />
    );
}