import { UnstyledButton } from "@mantine/core";
import { IconLoader } from "@tabler/icons-react";

interface SubmitButtonProps {
    isPending: boolean,
    disabled?: boolean
}

export default function SubmitButton({ isPending, disabled = false }: SubmitButtonProps) {
    return (
        <UnstyledButton
            className="flex flex-row min-w-full justify-between text-center rounded-md p-1 bg-standard-red text-standard-blue hover:opacity-65 disabled:opacity-65 disabled:cursor-not-allowed"
            type="submit"
            disabled={disabled || isPending}
        >
            {isPending ? <IconLoader className="animate-spin" /> : <div />}Submit<div />
        </UnstyledButton>
    );
}