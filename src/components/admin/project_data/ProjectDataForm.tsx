"use client";

import { Center, Flex, Space, } from "@mantine/core";
import ResponsiveTitle from "../../root/ResponsiveTitle";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { ProjectDataValues, projectDataSchema } from "@/lib/validation";
import { useCreateUpdateProjectData } from "@/hooks/data/creation+updating/useCreateUpdateProjectData";
import StandardAdminTextInput from "../StandardAdminTextInput";
import StandardAdminTextarea from "../StandardAdminTextarea";
import ProjectDataTypeSelector from "./ProjectDataTypeSelector";
import StandardAdminFieldset from "../StandardAdminFieldset";

interface ProjectDataFormProps {
    projectDataId?: string,
    onProjectDataIdChange: (newId: string) => void
}

export default function ProjectDataForm({ projectDataId, onProjectDataIdChange }: ProjectDataFormProps) {

    const { control, handleSubmit, reset, formState: { errors } } = useForm<ProjectDataValues>({
        resolver: zodResolver(projectDataSchema),
        defaultValues: {
            name: "",
            description: "",
            logoUrl: "",
            type: "ANDROID",
            gitlabUrl: "",
            accessUrl: ""
        },
    });

    const projectDataMutation = useCreateUpdateProjectData();

    function onSubmit(input: ProjectDataValues) {
        const refinedInput: ProjectDataValues = { ...input, accessUrl: input.accessUrl !== "" ? input.accessUrl : undefined }

        projectDataMutation.mutate(
            projectDataId ? { id: projectDataId, ...refinedInput } : refinedInput,
            {
                onSuccess: projectDataId ? undefined : ({ id }) => onProjectDataIdChange(id)
            }
        );
        reset();
    }

    return (
        <Flex
            className="bg-black p-5 rounded-xl shadow-xl shadow-gray-950"
            direction="column"
        >
            <Center>
                <ResponsiveTitle
                    title="PROJECT DATA"
                />
            </Center>
            <Space h={20} />
            <form
                onSubmit={handleSubmit(onSubmit)}
            >
                <StandardAdminFieldset
                    isPending={projectDataMutation.isPending}
                >
                    <StandardAdminTextInput
                        required
                        name="name"
                        label="Project name"
                        placeholder="Enter your Project name here"
                        control={control}
                        error={errors.name}
                    />
                    <StandardAdminTextarea
                        required
                        name="description"
                        label="Project description"
                        placeholder="Enter your Project description here"
                        control={control}
                        error={errors.description}
                    />
                    <StandardAdminTextInput
                        required
                        name="logoUrl"
                        label="Logo url"
                        placeholder="Enter your Logo url here"
                        control={control}
                        error={errors.logoUrl}
                    />
                    <ProjectDataTypeSelector
                        name="type"
                        label="Project Type"
                        control={control}
                    />
                    <StandardAdminTextInput
                        required
                        name="gitlabUrl"
                        label="Gitlab repository url"
                        prefixLabel="gitlab.com/MaskedRedstonerProZ"
                        prefix="https://gitlab.com/MaskedRedstonerProZ/"
                        prefixWidth={300}
                        placeholder="Enter your Gitlab url here"
                        control={control}
                        error={errors.gitlabUrl}
                    />
                    <StandardAdminTextInput
                        name="accessUrl"
                        label="Access url"
                        placeholder="Enter your Access url here"
                        control={control}
                        error={errors.accessUrl}
                    />
                </StandardAdminFieldset>
            </form>
        </Flex>
    );
}