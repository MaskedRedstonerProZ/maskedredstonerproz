"use client";

import { Flex, Menu, Space, UnstyledButton, rem } from "@mantine/core";
import { Icon, IconBraces, IconBrandAndroid, IconNetwork, IconProps } from "@tabler/icons-react";
import { ForwardRefExoticComponent, RefAttributes, useState } from "react";
import { Control, Controller } from "react-hook-form";

interface ProjectDataTypeSelectorProps {
    name: string,
    label?: string,
    control: Control<any, any>
}

interface TypeDisplay {
    icon: ForwardRefExoticComponent<IconProps & RefAttributes<Icon>>,
    label: string,
    slug: string
}

const types: TypeDisplay[] = [
    {
        icon: IconBrandAndroid,
        label: "Android",
        slug: "ANDROID"
    },
    {
        icon: IconNetwork,
        label: "Web",
        slug: "WEB"
    },
    {
        icon: IconBraces,
        label: "Other",
        slug: "OTHER"
    }
] satisfies TypeDisplay[]

export default function ProjectDataTypeSelector({ name, label, control }: ProjectDataTypeSelectorProps) {

    const [selected, setSelected] = useState(types[0]);

    const Icon = selected.icon;

    return (
        <Controller
            name={name}
            control={control}
            render={({ field }) => (
                <Menu
                    classNames={
                        {
                            dropdown: "bg-black p-0 border-standard-red",
                        }
                    }
                    width="target"
                >
                    <Flex
                        direction="column"
                    >
                        {label}
                        <Menu.Target>
                            <UnstyledButton
                                className="min-w-full border border-solid border-standard-red p-1 rounded-md"
                            >
                                <Flex
                                    direction="row"
                                >
                                    <Icon />
                                    <Space w={10} />
                                    <span>{selected.label}</span>
                                </Flex>
                            </UnstyledButton>
                        </Menu.Target>
                    </Flex>
                    <Menu.Dropdown>
                        {types.map(({ icon: Icon, label }, i) => (
                            <Menu.Item
                                key={i}
                                className="bg-black text-standard-white hover:border hover:border-solid hover:border-standard-red hover:bg-standard-red hover:text-standard-blue"
                                leftSection={<Icon style={{ height: rem(18), width: rem(18) }} stroke={1.5} />}
                                onClick={() => {
                                    const type = types[i]
                                    setSelected(type);
                                    field.onChange(type.slug);
                                }}
                            >
                                {label}
                            </Menu.Item>
                        ))}
                    </Menu.Dropdown>
                </Menu>
            )}
        />
    );
}