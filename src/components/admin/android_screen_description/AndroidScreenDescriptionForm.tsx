"use client";

import ResponsiveTitle from "@/components/root/ResponsiveTitle";
import { Center, Flex, Space } from "@mantine/core";
import StandardAdminFieldset from "../StandardAdminFieldset";
import StandardAdminTextInput from "../StandardAdminTextInput";
import StandardAdminTextarea from "../StandardAdminTextarea";
import { AndroidScreenDescriptionValues, androidScreenDescriptionSchema } from "@/lib/validation";
import { useForm } from "react-hook-form";
import { zodResolver } from "@hookform/resolvers/zod";
import { useCreateUpdateAndroidScreenDescription } from "@/hooks/data/creation+updating/useCreateUpdateAndroidScreenDescription";

interface AndroidScreenDescriptionFormProps {
    androidScreenDescriptionId?: string,
    androidAppDataId?: string,
    onAndroidScreenDescriptionIdChange: (newId: string) => void,
}

export default function AndroidScreenDescriptionForm({ androidScreenDescriptionId, androidAppDataId, onAndroidScreenDescriptionIdChange }: AndroidScreenDescriptionFormProps) {

    const { control, handleSubmit, reset, formState: { errors } } = useForm<AndroidScreenDescriptionValues>({
        resolver: zodResolver(androidScreenDescriptionSchema),
        defaultValues: {
            screenName: "",
            screenDescription: "",
            androidAppDataId: "i"
        },
    });

    const androidScreenDescriptionMutation = useCreateUpdateAndroidScreenDescription()

    function onSubmit(input: AndroidScreenDescriptionValues) {
        androidScreenDescriptionMutation.mutate(
            /*androidScreenDescriptionId ? { id: androidScreenDescriptionId, ...input, androidAppDataId: androidAppDataId!! } : */{ ...input, androidAppDataId: androidAppDataId!! },
            {
                onSuccess: androidScreenDescriptionId ? undefined : ({ id }) => onAndroidScreenDescriptionIdChange(id)
            }
        );
        reset();
    }

    return (
        <Flex
            className="bg-black p-5 rounded-xl shadow-xl shadow-gray-950"
            direction="column"
            flex={1}
        >
            <Center>
                <ResponsiveTitle
                    title="SCREEN DESCRIPTION"
                />
            </Center>
            <Space h={20} />
            <form
                onSubmit={handleSubmit(onSubmit)}
            >
                <StandardAdminFieldset
                    isPending={androidScreenDescriptionMutation.isPending}
                    submittingDisabled={!androidAppDataId}
                >
                    <StandardAdminTextInput
                        required
                        name="screenName"
                        label="Screen name"
                        placeholder="Enter your Screen name here"
                        control={control}
                        error={errors.screenName}
                    />
                    <StandardAdminTextarea
                        required
                        name="screenDescription"
                        label="Screen description"
                        placeholder="Enter your Screen description here"
                        control={control}
                        error={errors.screenDescription}
                    />
                </StandardAdminFieldset>
            </form>
        </Flex>
    );
}