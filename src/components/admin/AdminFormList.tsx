"use client";

import { Flex } from "@mantine/core";
import ProjectDataForm from "./project_data/ProjectDataForm";
import { useState } from "react";
import AndroidAppDataForm from "./android_app_data/AndroidAppDataForm";
import AndroidScreenDescriptionForm from "./android_screen_description/AndroidScreenDescriptionForm";

export default function AdminFormList() {

    const [projectDataId, setProjectDataId] = useState<string>();
    const [androidAppDataId, setAndroidAppDataId] = useState<string>();
    const [androidScreenDescriptionId, setAndroidScreenDescriptionId] = useState<string>();

    return (
        <Flex
            direction="row"
            columnGap={30}
        >
            <ProjectDataForm
                projectDataId={projectDataId}
                onProjectDataIdChange={setProjectDataId}
            />
            <Flex
                direction="column"
                rowGap={30}
                flex={1}
                justify="space-between"
            >
                <AndroidAppDataForm
                    androidAppDataId={androidAppDataId}
                    projectDataId={projectDataId}
                    onAndroidAppDataIdChange={setAndroidAppDataId}
                />
                <AndroidScreenDescriptionForm
                    androidScreenDescriptionId={androidScreenDescriptionId}
                    androidAppDataId={androidAppDataId}
                    onAndroidScreenDescriptionIdChange={setAndroidScreenDescriptionId}
                />
            </Flex>
        </Flex>
    );
}