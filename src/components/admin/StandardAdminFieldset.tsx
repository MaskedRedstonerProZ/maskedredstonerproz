import { Fieldset, Flex, Space } from "@mantine/core";
import SubmitButton from "./SubmitButton";

interface StandardAdminFieldsetProps extends React.PropsWithChildren {
    isPending: boolean,
    submittingDisabled?: boolean 
}

export default function StandardAdminFieldset({ isPending, submittingDisabled, children }: StandardAdminFieldsetProps) {
    return (
        <Fieldset
            className="bg-transparent border-standard-red"
        >
            <Flex
                direction="column"
                rowGap={10}
            >
                {children}
            </Flex>
            <Space h={20} />
            <SubmitButton
                isPending={isPending}
                disabled={submittingDisabled}
            />
        </Fieldset>
    );
}