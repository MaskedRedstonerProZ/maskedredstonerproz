import { Text, TextInput } from "@mantine/core";
import { useState } from "react";
import { Control, Controller, FieldError } from "react-hook-form";
import TextInputTest from "./TextInputTest";

interface StandardAdminTextInputProps {
    name: string,
    label?: string,
    prefixLabel?: string,
    prefix?: string,
    prefixWidth?: number,
    placeholder?: string,
    required?: boolean,
    control: Control<any, any>,
    error?: FieldError
}

export default function StandardAdminTextInput({ name, control, ...props }: StandardAdminTextInputProps) {

    return (
        <Controller
            name={name}
            control={control}
            render={({ field }) => (
                <TextInputTest
                    field={field}
                    {...props}
                />
            )}
        />
    );
}