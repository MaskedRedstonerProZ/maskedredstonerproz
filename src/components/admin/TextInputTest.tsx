import { Text, TextInput } from "@mantine/core";
import { useEffect, useState } from "react";
import { ControllerRenderProps, FieldError } from "react-hook-form";

interface TextInputTestProps {
    field: ControllerRenderProps<any, string>,
    label?: string,
    prefixLabel?: string,
    prefix?: string,
    prefixWidth?: number,
    placeholder?: string,
    required?: boolean,
    error?: FieldError
}

export default function TextInputTest({ field, label, prefixLabel, prefix, prefixWidth, placeholder, required = false, error }: TextInputTestProps) {

    const [value, setValue] = useState("");

    useEffect(() => {
        if ((field.value as string).length === 0) {
            field.onChange(`${prefix || ""}${value}`);
            setValue("");
        }
    }, [field, prefix, field.value]);

    useEffect(() => {
        if(value.length === 0) return;
        field.onChange(`${prefix || ""}${value}`);
    }, [value]);

    return (
        <TextInput
            classNames={
                {
                    input: "bg-black text-standard-white border-standard-red focus:border-standard-red",
                    section: "bg-black text-standard-white"
                }
            }
            label={label}
            placeholder={placeholder}
            value={value}
            required={required}
            error={error?.message}
            prefix={prefix}
            leftSectionWidth={prefixWidth}
            leftSection={prefixLabel || prefix ? (
                <div
                    className="flex flex-row items-center justify-center pr-3 h-full border-r border-r-standard-red border-l-transparent border-y-transparent border-solid"
                >
                    <Text
                        size="sm"
                    >
                        {prefixLabel || prefix}
                    </Text>
                </div>
            ) : undefined}
            onChange={(e) => {
                setValue(e.currentTarget.value)
            }}
            onKeyDown={(e) => e.stopPropagation()}
        />
    );
}