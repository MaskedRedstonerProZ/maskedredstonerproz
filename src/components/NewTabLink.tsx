"use client";

import contextMenuPreventor from "@/util/contextMenuPreventor";
import { MouseEvent } from "react";

interface NewTabLinkProps {
    href: string
}

export default function NewTabLink({ href, children }: React.PropsWithChildren<NewTabLinkProps>) {

    function handleMailto(e: MouseEvent<HTMLAnchorElement>) {
        if (e.currentTarget.href.startsWith("mailto")) {
            e.preventDefault();
            window.open(e.currentTarget.href);
        }
    }

    return (
        <a
            className="text-standard-white pl-3 no-underline hover:underline"
            href={href}
            target="_blank"
            rel="noopener noreferer"
            onClick={handleMailto}
            onContextMenu={contextMenuPreventor}
        >
            {children}
        </a>
    );
}