"use client";

import { Space, Text } from "@mantine/core";
import ResponsiveTitle from "./ResponsiveTitle";
import Linkify from "../Linkify";
import Logo from "../Logo";
import ExpandingText from "./ExpandingText";
import { useState } from "react";

export default function Header() {

    const [shouldTextExpand, setShouldTextExpand] = useState(false);

    function toggleShouldTextExpand() {
        setShouldTextExpand((prevState) => !prevState);
    }

    return (
        <div
            className="flex flex-col items-center bg-black max-w-full p-5 rounded-xl shadow-xl shadow-gray-950"
            onClick={toggleShouldTextExpand}
        >
            <div
                className="flex flex-col items-center pb-5 md:flex-row"
            >
                <Logo className="select-none" />
                <div
                    className="flex flex-col pt-5 items-center md:pl-5 md:pt-0"
                >
                    <Text
                        className="xs:hidden"
                    >
                        MaskedRedstonerProZ
                    </Text>
                    <div
                        className="hidden xs:block"
                    >
                        <ResponsiveTitle
                            title="MaskedRedstonerProZ"
                        />
                    </div>
                    <Space h="lg" />
                    <Text
                        className="text-wrap select-none"
                    >
                        I&apos;m an Android developer with over three years of experience, complemented by two years in web development.<br /><br />
                        My skillset includes not only building user-friendly interfaces but also creating reliable backends for Android apps and websites,
                        ensuring fully integrated and seamless experiences.
                    </Text>
                </div>
            </div>
            <Linkify>
                <ExpandingText
                    shouldClamp={!shouldTextExpand}
                >
                    In my work, I&apos;m dedicated to consistency and high standards across both design and code.
                    If a button is red with blue text in one project, it&apos;ll be the same across all of them,
                    with code held to the same level of precision. This attention to detail brings a cohesive, familiar feel to each project.<br /><br />
                    For Android apps and backends, I rely on Kotlin to bring efficiency and precision,
                    while for websites, I use Next.js with TypeScript to ensure a smooth and responsive experience.
                    I plan to document my journey and future projects on Instagram at @maskedredstonerproz.
                </ExpandingText>
            </Linkify>
        </div>
    );
}