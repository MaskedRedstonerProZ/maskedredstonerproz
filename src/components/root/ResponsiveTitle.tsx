"use client";

import { Title, TitleOrder } from "@mantine/core";
import { Orbitron } from "next/font/google";
import { useMobile } from "@/hooks/useMobile";

const font = Orbitron({ weight: "400", subsets: ["latin"] });

interface ResponsiveTitleProps {
    title: string,
    mobileOrder?: TitleOrder,
    desktopOrder?: TitleOrder,
    mobileSize?: number,
    desktopSize?: number,
}

export default function ResponsiveTitle({ title, mobileOrder = 2, desktopOrder = 1, mobileSize = undefined, desktopSize = undefined }: ResponsiveTitleProps) {
    
    const isMobile = useMobile();
    
    return (
        <Title
            order={isMobile ? mobileOrder : desktopOrder}
            size={isMobile ? mobileSize || undefined : desktopSize || undefined}
            className={font.className + " select-none"}
        >
            {title}
        </Title>
    );
}