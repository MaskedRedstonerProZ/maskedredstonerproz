import { Tabs } from "@mantine/core";

interface StandardTabsProps extends React.PropsWithChildren {
    defaultValue?: number,
    value?: number,
    onValueChanged?: (newValue?: number) => void,
}

export default function StandardTabs({ defaultValue = 0, value, children, onValueChanged }: StandardTabsProps) {
    return (
        <Tabs
            classNames={
                {
                    tab: "hover:bg-standard-red hover:text-standard-blue data-active:bg-standard-red data-active:text-standard-blue",
                    root: "flex flex-col flex-grow justify-center items-center"
                }
            }
            color="#c80000"
            variant="pills"
            radius="xl"
            defaultValue={defaultValue.toString()}
            value={value?.toString()}
            onChange={(pos) => onValueChanged && onValueChanged(Number(pos))}
        >
            {children}
        </Tabs>
    );
}