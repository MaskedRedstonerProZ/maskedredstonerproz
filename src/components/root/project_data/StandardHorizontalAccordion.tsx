import { Flex, Space, rem } from "@mantine/core";
import { Icon, IconProps } from "@tabler/icons-react";
import { ForwardRefExoticComponent, RefAttributes } from "react";
import StandardHorizontalAccordionItem from "./StandardHorizontalAccordionItem";

interface StandardHorizontalAccordionProps {
    icons: ForwardRefExoticComponent<IconProps & RefAttributes<Icon>>[],
    accordionItems: string[],
    fullLinks: string[]
}

export default function StandardHorizontalAccordion({ icons, accordionItems, fullLinks }: StandardHorizontalAccordionProps) {
    return (
        <Flex
            direction="row"
            align="center"
        >
            {icons.map((icon, i) => {

                const accordionItem = accordionItems[i];
                const fullLink = fullLinks[i];

                return (
                    <Flex
                        key={i}
                        direction="row"
                        align="center"
                    >
                        <StandardHorizontalAccordionItem
                            icon={icon}
                            accordionItem={accordionItem}
                            fullLink={fullLink}
                            shouldShowByDefault={icons.length === 1}
                            isLinkFileDownload={i === 1}
                        />
                        {icons.length - 1 !== i && <Space w={10} />}
                    </Flex>
                )
            })}
        </Flex>
    );
}