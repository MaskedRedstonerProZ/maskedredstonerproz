"use client";

import { Flex, Space, Tabs, rem } from "@mantine/core";
import Image from "next/image";
import ScreenshotCarousel from "../ScreenshotCarousel";
import { useMobile } from "@/hooks/useMobile";
import { useState } from "react";
import ResponsiveTitle from "../../ResponsiveTitle";
import ExpandingText from "../../ExpandingText";
import { IconBrandAndroid, IconBrandGit } from "@tabler/icons-react";
import clsx from "clsx";
import { usePhone } from "@/hooks/usePhone";
import { useTablet } from "@/hooks/useTablet";
import StandardHorizontalAccordion from "../StandardHorizontalAccordion";
import { Carousel } from "@mantine/carousel";
import StandardTabs from "../../StandardTabs";
import { AndroidAppData, ProjectData } from "@/lib/types";

interface AppDataCardProps {
    androidAppData: AndroidAppData,
    name: string,
    description: string,
    appLogoSize: number,
    logoUrl: string,
    gitlabUrl: string,
    accessUrl: string | null,
}

export default function AppDataCard({ androidAppData, name, description, appLogoSize, logoUrl, gitlabUrl, accessUrl }: AppDataCardProps) {

    const isMobile = useMobile();

    const [screenPosition, setScreenPosition] = useState(0);

    return (
        <Flex
            className="bg-black rounded-xl p-5 shadow-xl shadow-gray-950 min-h-[70vh] min-w-full"
            direction="column"
            justify="space-between"
        >
            {!isMobile ? (
                <DesktopAppDataCard
                    screenPosition={screenPosition}
                    data={androidAppData}
                    projectName={name}
                    projectDescription={description}
                    appLogoSize={appLogoSize}
                    appLogoUrl={logoUrl}
                    onScreenPositionChange={setScreenPosition}
                />
            ) : (
                <MobileAppDataCard
                    screenPosition={screenPosition}
                    data={androidAppData}
                    projectName={name}
                    projectDescription={description}
                    appLogoSize={appLogoSize}
                    appLogoUrl={logoUrl}
                    onScreenPositionChange={setScreenPosition}
                />
            )}
            <StandardHorizontalAccordion
                icons={
                    accessUrl ? [
                        IconBrandGit,
                        IconBrandAndroid
                    ] : [
                        IconBrandGit
                    ]
                }
                accordionItems={
                    [
                        name,
                        name
                    ]
                }
                fullLinks={
                    [
                        gitlabUrl,
                        accessUrl || ""
                    ]
                }
            />
        </Flex>
    );
}

interface PlatformAppDataCardProps {
    screenPosition: number,
    data: AndroidAppData,
    projectName: string,
    projectDescription: string,
    appLogoSize: number,
    appLogoUrl: string,
    onScreenPositionChange: (it: number) => void,
}

function DesktopAppDataCard({ screenPosition, data, projectName, projectDescription, appLogoSize, appLogoUrl, onScreenPositionChange }: PlatformAppDataCardProps) {
    return (
        <Flex
            direction="column"
            align="center"
            rowGap={5}
        >
            <Image
                src={appLogoUrl}
                alt="app logo"
                width={appLogoSize}
                height={appLogoSize}
            />
            <ResponsiveTitle
                title={projectName}
                mobileSize={40}
                desktopSize={60}
            />
            <Flex
                direction="row"
            >
                <Flex
                    direction="column"
                >
                    <ExpandingText shouldClamp={false} >
                        {projectDescription}
                    </ExpandingText>
                    <Space h={20} />
                    <ExpandingText shouldClamp={false} >
                        {data.screenDescriptions[screenPosition].screenName}
                    </ExpandingText>
                    <Space h={20} />
                    <ExpandingText shouldClamp={false} >
                        {data.screenDescriptions[screenPosition].screenDescription}
                    </ExpandingText>
                </Flex>
                <ScreenshotCarousel
                    screenshots={data.screenshotUrls || []}
                    onPositionChange={onScreenPositionChange}
                />
            </Flex>
        </Flex>
    );
}

function MobileAppDataCard({ screenPosition, data, projectName, projectDescription, appLogoSize, appLogoUrl, onScreenPositionChange }: PlatformAppDataCardProps) {

    const isPhone = usePhone();
    const isTablet = useTablet();

    const [activeTab, setActiveTab] = useState(0);

    function handleTabChange(index: number) {
        setActiveTab(index);
    }

    return (
        <StandardTabs
            defaultValue={0}
            value={activeTab}
            onValueChanged={(pos) => {
                onScreenPositionChange(0);
                handleTabChange(pos || 0);
            }}
        >
            <Tabs.List className="min-w-full" grow justify="center">
                <Tabs.Tab
                    value="0"
                >
                    App
                </Tabs.Tab>
                <Tabs.Tab
                    value="1"
                >
                    Display
                </Tabs.Tab>
            </Tabs.List>

            <Carousel
                slideSize="100%"
                slideGap={rem(0)}
                withControls={false}
                withIndicators={false}
                align="start"
                initialSlide={activeTab}
                withKeyboardEvents={false}
                loop
                onSlideChange={(index) => {
                    onScreenPositionChange(0)
                    handleTabChange(index)
                }}
            >
                <Carousel.Slide>
                    <Tabs.Panel
                        className="flex flex-col min-h-[70vh] items-center justify-center"
                        value="0"
                    >
                        {isTablet && <Space h={20} />}
                        <Image
                            src={appLogoUrl}
                            alt="app logo"
                            width={appLogoSize}
                            height={appLogoSize}
                        />
                        <Space h={20} />
                        <ResponsiveTitle
                            title={projectName}
                            mobileSize={40}
                            desktopSize={60}
                        />
                        <Space h={20} />
                        <ExpandingText shouldClamp={false} >
                            {projectDescription}
                        </ExpandingText>
                        {isTablet && <Space h={20} />}
                    </Tabs.Panel>
                </Carousel.Slide>
                <Carousel.Slide>
                    <Tabs.Panel
                        className={
                            clsx(
                                "flex items-center justify-center min-h-[70vh]",
                                isPhone ? "flex-col" : "flex-row"
                            )
                        }
                        value="1"
                    >
                        {isTablet && <Space h={20} />}
                        <div
                            className="flex flex-col items-center"
                        >
                            <ExpandingText shouldClamp >
                                {data.screenDescriptions[screenPosition].screenName}
                            </ExpandingText>
                            <Space h={20} />
                            <ExpandingText shouldClamp >
                                {data.screenDescriptions[screenPosition].screenDescription}
                            </ExpandingText>
                        </div>
                        <div
                            className="flex flex-col items-center"
                        >
                            <Space h={20} />
                            <ScreenshotCarousel
                                screenshots={data.screenshotUrls || []}
                                position={screenPosition}
                                onPositionChange={onScreenPositionChange}
                            />
                        </div>
                    </Tabs.Panel>
                </Carousel.Slide>
            </Carousel>
        </StandardTabs>
    )
}