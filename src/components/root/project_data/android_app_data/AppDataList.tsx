"use client";

import { Flex } from "@mantine/core";
import AppDataCard from "./AppDataCard";
import { ProjectData } from "@/lib/types";

interface AppDataListProps {
    data: ProjectData[]
}

export default function AppDataList({ data }: AppDataListProps) {

    const projectDataWithNonNullAndroidAppData = data.filter(({ androidAppData }) => androidAppData !== null).map((projectData) => (
        {
            ...projectData,
            androidAppData: projectData.androidAppData!!
        } satisfies ProjectData
    ));

    return (
        <Flex
            direction="column"
            align="center"
            gap={30}
        >
            {projectDataWithNonNullAndroidAppData.map((dataItem, i) => (
                <AppDataCard
                    key={i}
                    {...dataItem}
                    appLogoSize={250}
                />
            ))}
        </Flex>
    );
}