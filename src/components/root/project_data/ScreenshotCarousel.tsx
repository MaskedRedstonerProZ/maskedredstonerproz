"use client";

import { Carousel } from "@mantine/carousel";
import Image from "next/image";
import "@mantine/carousel/styles.css";

interface ScreenshotCarouselProps {
    screenshots: string[],
    position?: number,
    onPositionChange: (newPos: number) => void,
}

export default function ScreenshotCarousel({ screenshots, position, onPositionChange }: ScreenshotCarouselProps) {

    const slides = screenshots.map((screenshot, i) => (
        <Carousel.Slide
            key={i}
            className="flex flex-row justify-center"
        >
            <Image
                className="border-4 border-solid border-standard-red-important border-opacity-75 rounded-md"
                src={screenshot}
                alt="screenshot"
                width={200}
                height={504}
            />
        </Carousel.Slide>
    ));

    return (
        <Carousel
            classNames={
                {
                    control: "bg-standard-red-important text-standard-blue border border-solid border-standard-red-important size-8"
                }
            }
            withControls
            h={504}
            w={300}
            draggable={false}
            loop
            initialSlide={position}
            onSlideChange={onPositionChange}
        >
            {slides}
        </Carousel>
    );
}