"use client";

import { Center, ScrollArea, Tabs } from "@mantine/core";
import StandardTabs from "../StandardTabs";
import AppDataList from "./android_app_data/AppDataList";
import { IconLoader } from "@tabler/icons-react";
import { useGetProjectData } from "@/hooks/data/fetching/useGetProjectData";
import { ProjectData } from "@/lib/types";

export default function ProjectDataList() {

    // const appData = useAppData();

    const { data, hasNextPage, isFetching, isFetchingNextPage, fetchNextPage } = useGetProjectData();

    const projectDataList = data?.pages.flatMap((page) => page.projectData) || [];

    const androidProjectData: ProjectData[] = projectDataList.filter((projectData) => projectData.type === "ANDROID");

    return (
        <StandardTabs>
            <Tabs.List className="bg-black p-3 rounded-full shadow-xl shadow-gray-950 min-w-full" grow justify="center">
                <Tabs.Tab
                    value="0"
                >
                    Android
                </Tabs.Tab>
                <Tabs.Tab
                    value="1"
                >
                    Web
                </Tabs.Tab>
                <Tabs.Tab
                    value="2"
                >
                    Others
                </Tabs.Tab>
            </Tabs.List>

            <Tabs.Panel
                value="0"
                className="pt-[30px] min-w-full"
            >
                {isFetching ? (
                    <IconLoader className="animate-spin mx-auto" color="black" />
                ) : androidProjectData.length > 0 ? (
                    <ScrollArea
                        onBottomReached={() => hasNextPage && !isFetching && fetchNextPage()}
                    >
                        <AppDataList
                            data={androidProjectData}
                        />
                        {isFetchingNextPage && <IconLoader className="animate-spin mx-auto" color="black" />}
                    </ScrollArea>
                ) : (
                    <Center className="bg-black rounded-full py-2 shadow-xl shadow-gray-950" >Error 404: Projects not found!</Center>
                )}
            </Tabs.Panel>

            <Tabs.Panel
                value="1"
                className="pt-[30px] min-w-full"
            >
                <Center className="bg-black rounded-full py-2 shadow-xl shadow-gray-950" >Error 404: Projects not found!</Center>
            </Tabs.Panel>

            <Tabs.Panel
                value="2"
                className="pt-[30px] min-w-full"
            >
                <Center className="bg-black rounded-full py-2 shadow-xl shadow-gray-950" >Error 404: Projects not found!</Center>
            </Tabs.Panel>

        </StandardTabs>
    );
}