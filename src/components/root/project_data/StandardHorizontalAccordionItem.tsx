"use client";

import { Flex, Space, Text, rem } from "@mantine/core";
import { Icon, IconProps } from "@tabler/icons-react";
import { ForwardRefExoticComponent, RefAttributes, useState } from "react";
import NewTabLink from "../../NewTabLink";
import contextMenuPreventor from "@/util/contextMenuPreventor";

interface StandardHorizontalAccordionItemProps {
    icon: string | ForwardRefExoticComponent<IconProps & RefAttributes<Icon>>,
    accordionItem: string,
    fullLink: string,
    shouldShowByDefault?: boolean,
    isLinkFileDownload?: boolean
}

export default function StandardHorizontalAccordionItem({ icon, accordionItem, fullLink, shouldShowByDefault = false, isLinkFileDownload = false }: StandardHorizontalAccordionItemProps) {

    const Icon = icon

    const [isShown, setIsShown] = useState(shouldShowByDefault);

    function toggleIsShown() {
        setIsShown((prevState) => !prevState);
    }

    return (
        <Flex
            direction="row"
            align="center"
        >
            <Icon
                className="cursor-pointer"
                style={
                    {
                        width: rem(48),
                        height: rem(48),
                        color: "#F7F0F5"
                    }
                }
                stroke={1.5}
                onClick={toggleIsShown}
            />
            <Space w={5} />
            {isLinkFileDownload ? (
                <a
                    href={fullLink}
                    className="text-standard-white hover:text-standard-white no-underline hover:underline"
                    onContextMenu={contextMenuPreventor}
                >
                    {isShown && <Text>{accordionItem}</Text>}
                </a>
            ) : (
                <NewTabLink
                    href={fullLink}
                >
                    {isShown && <Text>{accordionItem}</Text>}
                </NewTabLink>
            )}
        </Flex>
    );
}