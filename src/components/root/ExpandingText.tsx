"use client";

import { Text } from "@mantine/core";
import clsx from "clsx";
import React, { useEffect, useState } from "react";

interface ExpandingTextProps extends React.PropsWithChildren {
    shouldClamp: boolean
}

export default function ExpandingText({ shouldClamp, children }: ExpandingTextProps) {

    const [isClamping, setIsClamping] = useState(shouldClamp);

    useEffect(() => {
        setIsClamping(shouldClamp);
    }, [shouldClamp, children]);

    return (
        <Text
            className={clsx(
                `text-wrap select-none`,
                isClamping && `line-clamp-3`
            )}
            onClick={() => shouldClamp && setIsClamping((prevState) => !prevState)}
        >
            {children}
        </Text>
    );
}