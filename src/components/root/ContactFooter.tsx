import { Flex, rem } from "@mantine/core";
import { IconBrandBluesky, IconBrandGitlab, IconBrandGmail, IconBrandInstagram, IconBrandThreads, IconBrandTiktok, IconProps } from "@tabler/icons-react";
import NewTabLink from "../NewTabLink";
import StandardTooltip from "../StandardTooltip";

export default function ContactFooter() {

    const iconSize = 48

    const icons = [
        IconBrandGitlab,
        IconBrandGmail,
        IconBrandInstagram,
        IconBrandBluesky,
        IconBrandTiktok,
        IconBrandThreads
    ]

    function SocialLinks() {

        const iconStyle = {
            style: {
                width: rem(iconSize),
                height: rem(iconSize),
                color: "#F7F0F5"
            },
            stroke: 1.5
        } as IconProps

        return (
            <>
                {
                    [
                        "https://gitlab.com/MaskedRedstonerProZ",
                        "mailto:maskedredstonerproz@gmail.com",
                        "https://instagram.com/maskedredstonerproz",
                        "https://bsky.app/profile/maskedredstoner.bsky.social",
                        "https://www.tiktok.com/@maskedredstonerproz",
                        "https://www.threads.net/@maskedredstonerproz"
                    ].map((link, i) => {

                        const Icon = icons[i]

                        const secondLinkSegment = link.split("/")[2]

                        const secondLinkSegmentSegments = secondLinkSegment?.split(".") || ["email"];

                        const tooltipLabel = link.startsWith("mailto")
                            ?
                            "gmail"
                            :
                            secondLinkSegment.startsWith("www")
                                ?
                                secondLinkSegmentSegments[1]
                                :
                                secondLinkSegmentSegments[0] === "bsky"
                                    ?
                                    "Bluesky"
                                    :
                                    secondLinkSegmentSegments[0]

                        return (
                            <NewTabLink
                                key={i}
                                href={link}
                            >
                                <StandardTooltip
                                    label={`${tooltipLabel[0].toUpperCase()}${tooltipLabel.substring(1)}`}
                                >
                                    <Icon
                                        className="select-none"
                                        style={iconStyle.style}
                                        stroke={iconStyle.stroke}
                                    />
                                </StandardTooltip>
                            </NewTabLink>
                        )
                    })
                }
            </>
        );
    }

    return (
        <Flex
            className="bg-black rounded-xl shadow-xl shadow-gray-950"
            direction="row"
            p={20}
            align="center"
            justify="space-evenly"
        >
            <SocialLinks />
        </Flex>
    );
}