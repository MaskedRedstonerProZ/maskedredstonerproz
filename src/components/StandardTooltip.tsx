import { Tooltip } from "@mantine/core";

interface StandardTooltipProps extends React.PropsWithChildren {
    label: string,
    altColour?: boolean
}

export default function StandardTooltip({ label, altColour = false, children }: StandardTooltipProps) {
    return (
        <Tooltip
            label={label}
            color={altColour ? "black" : "#30343F"}
            events={
                {
                    hover: true,
                    focus: true,
                    touch: true
                }
            }
            position={altColour ? "left" : "top"}
        >
            <div>
                {children}
            </div>
        </Tooltip>
    );
}