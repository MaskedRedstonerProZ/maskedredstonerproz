import { Prisma } from "@prisma/client";

const androidAppDataInclude = {
    screenDescriptions: true
} satisfies Prisma.AndroidAppDataInclude;

export const projectDataInclude = {
    androidAppData: {
        include: androidAppDataInclude
    }
} satisfies Prisma.ProjectDataInclude;

export type ProjectData = Prisma.ProjectDataGetPayload<{
    include: typeof projectDataInclude
}>;

export type AndroidAppData = Prisma.AndroidAppDataGetPayload<{
    include: typeof androidAppDataInclude
}>;

export interface ProjectDataPage {
	projectData: ProjectData[];
	nextCursor: string | null;
}