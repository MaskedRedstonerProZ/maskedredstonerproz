import { PrismaClient } from "@prisma/client";

function prismaClientSkeleton() {
    return new PrismaClient();
}

declare global {
    var prismaGlobal: undefined | ReturnType<typeof prismaClientSkeleton>;
}

const prisma = globalThis.prismaGlobal ?? prismaClientSkeleton();

export default prisma;

if(process.env.NODE_ENV !== "production") globalThis.prismaGlobal = prisma;