import z from "zod";

const requiredString = z.string().trim().min(1, "Required!");
const optionalString = z.string().optional();

export const projectDataSchema = z.object(
    {
        id: optionalString,
        name: requiredString,
        description: requiredString,
        logoUrl: requiredString,
        type: z.enum(["ANDROID", "WEB", "OTHER"]),
        gitlabUrl: requiredString,
        accessUrl: optionalString
    }
);

export const androidAppDataSchema = z.object(
    {
        id: optionalString,
        screenshotUrls: z.array(requiredString).nonempty(),
        projectDataId: requiredString
    }
);

export const androidScreenDescriptionSchema = z.object(
    {
        id: optionalString,
        screenName: requiredString,
        screenDescription: requiredString,
        androidAppDataId: requiredString
    }
)

export type ProjectDataValues = z.infer<typeof projectDataSchema>;

export type AndroidAppDataValues = z.infer<typeof androidAppDataSchema>;

export type AndroidScreenDescriptionValues = z.infer<typeof androidScreenDescriptionSchema>;
