import { InfiniteData, UseInfiniteQueryResult, useInfiniteQuery } from "@tanstack/react-query";
import * as ProjectDataApi from "@/network/Api";
import { ProjectDataPage } from "@/lib/types";

export function useGetProjectData(): UseInfiniteQueryResult<InfiniteData<ProjectDataPage, unknown>, Error> {

    const query = useInfiniteQuery(
        {
            queryKey: ["project-data"],
            queryFn: ({ pageParam }) => ProjectDataApi.getProjectData(pageParam),
            initialPageParam: null as string | null,
            getNextPageParam: (lastPage) => lastPage.nextCursor,
            staleTime: 1000 * 60 * 60 * 24 * 3
        }
    );

    return query;
}