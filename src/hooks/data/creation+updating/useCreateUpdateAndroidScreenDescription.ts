import { AndroidScreenDescriptionValues } from "@/lib/validation";
import { UseMutationResult, useMutation } from "@tanstack/react-query";
import * as ProjectDataApi from "@/network/Api";

export function useCreateUpdateAndroidScreenDescription(): UseMutationResult<ProjectDataApi.IdResponse, Error, AndroidScreenDescriptionValues> {
    return useMutation(
        {
            mutationFn: async (values: AndroidScreenDescriptionValues) => await ProjectDataApi.createUpdateAndroidScreenDescription(values),
            onError(error) {
                console.error(error);
                alert(error);
            },
        }
    );
}