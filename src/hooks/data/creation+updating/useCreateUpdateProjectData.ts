import { ProjectDataValues } from "@/lib/validation";
import { UseMutationResult, useMutation } from "@tanstack/react-query";
import * as ProjectDataApi from "@/network/Api";

export function useCreateUpdateProjectData(): UseMutationResult<ProjectDataApi.IdResponse, Error, ProjectDataValues> {
    return useMutation(
        {
            mutationFn: async (values: ProjectDataValues) => await ProjectDataApi.createUpdateProjectData(values),
            onError(error) {
                console.error(error);
                alert(error);
            },
        }
    );
}