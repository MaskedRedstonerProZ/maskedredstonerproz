import { AndroidAppDataValues } from "@/lib/validation";
import { UseMutationResult, useMutation } from "@tanstack/react-query";
import * as ProjectDataApi from "@/network/Api";

export function useCreateUpdateAndroidAppData(): UseMutationResult<ProjectDataApi.IdResponse, Error, AndroidAppDataValues> {
    return useMutation(
        {
            mutationFn: async (values: AndroidAppDataValues) => await ProjectDataApi.createUpdateAndroidAppData(values),
            onError(error) {
                console.error(error);
                alert(error);
            },
        }
    );
}