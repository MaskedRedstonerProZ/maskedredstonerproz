import { useMantineTheme } from "@mantine/core";
import { useMediaQuery } from "@mantine/hooks";
import { useMemo } from "react";

export function usePhone(): boolean {
    const theme = useMantineTheme();
    
    const isPhone = !!useMediaQuery(`(max-width: ${theme.breakpoints.sm})`);

    return useMemo(() => isPhone, [isPhone]);
}