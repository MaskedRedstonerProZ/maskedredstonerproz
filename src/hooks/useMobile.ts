import { useMemo } from "react";
import { usePhone } from "./usePhone";
import { useTablet } from "./useTablet";

export function useMobile(): boolean {

    const isPhone = usePhone();
    const isTablet = useTablet();

    return useMemo(() => isPhone || isTablet, [isPhone, isTablet]);
}