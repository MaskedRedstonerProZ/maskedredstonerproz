import { useMantineTheme } from "@mantine/core";
import { useMediaQuery } from "@mantine/hooks";
import { useMemo } from "react";

export function useTablet(): boolean {
    const theme = useMantineTheme();
    
    const isTablet = !!useMediaQuery(`(max-width: ${theme.breakpoints.lg})`);

    return useMemo(() => isTablet, [isTablet]);
}