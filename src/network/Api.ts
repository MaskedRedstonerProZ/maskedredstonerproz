import { ProjectData, ProjectDataPage } from "@/lib/types";
import { AndroidAppDataValues, AndroidScreenDescriptionValues, ProjectDataValues } from "@/lib/validation";

export interface IdResponse {
    id: string
}

async function fetchData(input: RequestInfo, init?: RequestInit) {
    const response = await fetch(input, init);
    if (response.ok) {
        return response;
    } else {
        const errorBody = await response.json();
        const errorMessage = errorBody.message;
        throw Error("Request failed with status: " + response.status + " message: " + errorMessage);
    }
}

export async function getProjectData(cursor: string | null): Promise<ProjectDataPage> {

    const response = await fetchData(
        `/api/project_data/?cursor=${encodeURIComponent(cursor || "")}`,
        { 
            method: "GET"
        }
    );

    return response.json();
}

export async function createUpdateProjectData(request: ProjectDataValues): Promise<IdResponse> {

    const response = await fetchData(
        "/api/project_data",
        {
            method: "POST",
            body: JSON.stringify(request)
        }
    );

    return response.json();
}

export async function createUpdateAndroidAppData(request: AndroidAppDataValues): Promise<IdResponse> {

    const response = await fetchData(
        "/api/android_app_data",
        {
            method: "POST",
            body: JSON.stringify(request)
        }
    );

    return response.json();
}

export async function createUpdateAndroidScreenDescription(request: AndroidScreenDescriptionValues): Promise<IdResponse> {

    const response = await fetchData(
        "/api/android_screen_description",
        {
            method: "POST",
            body: JSON.stringify(request)
        }
    );

    return response.json();
}