import { MouseEvent } from "react";

export default function contextMenuPreventor<T>(e: MouseEvent<T>) {
    e.preventDefault();
}