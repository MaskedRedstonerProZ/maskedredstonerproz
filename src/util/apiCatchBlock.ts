
export default function apiCatchBlock(error: Error): Response {
    console.error(error);
    return Response.json(
        { error: process.env.NODE_ENV === "production" ? "Internal server error" : error },
        { status: 500 },
    );
}